package com.lenci.test.getyourguide.api

import android.support.test.runner.AndroidJUnit4
import com.lenci.test.getyourguide.api.SORT_DIRECTION.*
import com.lenci.test.getyourguide.api.SORT_TYPE.*
import com.lenci.test.getyourguide.utils.assertNonNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class TestApi {

    lateinit var api: Api

    @Before
    fun initApi() {
        api = Api()
    }

    @Test
    fun api_page_size() {
        var response = api.service.loadTourReviews().execute().body()
        assertNonNull(response) { it.reviews.size <= Constants.DEFAULT_PAGE_SIZE }

        response = api.service.loadTourReviews(page_size = 20).execute().body()
        assertNonNull(response) { it.reviews.size <= 20 }

        response = api.service.loadTourReviews(page_size = 1).execute().body()
        assertNonNull(response) { it.reviews.size == 1 }
    }

    @Test
    fun api_sort() {
        var response = api.service.loadTourReviews(sortType = WHEN, sortDirection = DESC).execute().body()

        assertNonNull(response) {

            for(i in 1 until it.reviews.size) {
                if(it.reviews[i].getDateTimestamp() > it.reviews[i - 1].getDateTimestamp()) {
                    return@assertNonNull false
                }
            }

            true
        }

        response = api.service.loadTourReviews(sortType = RATING, sortDirection = DESC).execute().body()
        assertNonNull(response) {

            for(i in 1 until it.reviews.size) {
                if(it.reviews[i].rating > it.reviews[i - 1].rating) {
                    return@assertNonNull false
                }
            }

            true
        }

        response = api.service.loadTourReviews(sortType = RATING, sortDirection = DESC).execute().body()
        assertNonNull(response) {

            for(i in 1 until it.reviews.size) {
                if(it.reviews[i].rating < it.reviews[i - 1].rating) {
                    return@assertNonNull false
                }
            }

            true
        }

    }

    @Test
    fun api_filter_rating() {
        val rating = 5
        val response = api.service.loadTourReviews(rating = rating, page_size = 50).execute().body()
        assertNonNull(response) {
            for(review in it.reviews) {
                if(review.rating != rating) {
                    return@assertNonNull false
                }
            }

            true
        }
    }

    @Test
    fun api_parse_everything() {
        var response = api.service.loadTourReviews(page_size = 1).execute().body()
        val count = response?.total_reviews_comments ?:0
        response = api.service.loadTourReviews(page_size = count).execute().body()

        assertNonNull(response) { count == it.total_reviews_comments }
    }

}
