package com.lenci.test.getyourguide

import android.app.Application
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
open class ContextTest {

    lateinit var context: Context
    lateinit var application: Application

    @Before
    open fun setup() {
        context = InstrumentationRegistry.getContext()
        application = context.applicationContext as Application
    }

}