package com.lenci.test.getyourguide.utils

fun <T: Any> assertNonNull(response: T?, toAssert: (it: T) -> Boolean) {
    assert(response?.let { toAssert(it) } ?: false)
}