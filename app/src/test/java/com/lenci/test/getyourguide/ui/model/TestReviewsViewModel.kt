package com.lenci.test.getyourguide.ui.model

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.*
import com.lenci.test.getyourguide.ContextTest
import com.lenci.test.getyourguide.ui.GetYourGuideApplication
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel.Status
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import com.lenci.test.getyourguide.api.Api
import com.lenci.test.getyourguide.api.SORT_DIRECTION
import com.lenci.test.getyourguide.api.SORT_TYPE
import com.lenci.test.getyourguide.ui.thread.TaskManager
import org.junit.Before
import org.robolectric.RobolectricTestRunner
import org.junit.rules.TestRule
import org.junit.Rule



@RunWith(RobolectricTestRunner::class)
class TestReviewsViewModel : ContextTest() {

    ////////////////////////////////////////
    // RULES
    ///////////////////////////////////////

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    lateinit var viewModel: ReviewsViewModel
    lateinit var api: Api
    lateinit var taskManager: TaskManager

    ////////////////////////////////////////
    // SETUP
    ///////////////////////////////////////

    @Before
    override fun setup() {
        super.setup()
        api = GetYourGuideApplication.instance.api
        taskManager = GetYourGuideApplication.instance.taskManager
    }

    ////////////////////////////////////////
    // UTILS
    ///////////////////////////////////////

    private fun <T> LiveData<T>.blockingObserve(count : Int = 1, task: () -> Unit = {}): T? {
        var value: T? = null
        val latch = CountDownLatch(count)
        val innerObserver = Observer<T> {
            value = it
            latch.countDown()
        }

        observeForever(innerObserver)
        task()
        assert(latch.await(5, TimeUnit.SECONDS))
        removeObserver(innerObserver)

        return value
    }


    private fun initViewModel() : ReviewsViewModel{
        val viewModel =  ReviewsViewModel(application, api, taskManager)
        assert(viewModel.status.value != null)
        assert(viewModel.reviews.value != null)

        //WAIT INIT
        viewModel.status.blockingObserve(2)

        return viewModel
    }

    private fun testLoadNext(): Int {
        val expectedPage = when(viewModel.status.value) {
            is Status.NotInitialized -> {
                0
            }
            is Status.Idle -> {
                viewModel.page() + 1
            }
            is Status.Error -> {
                viewModel.page()
            }
            else -> {
                -1
            }
        }

        assert(expectedPage != -1)

        //idle, loading, idle/error
        val status = viewModel.status.blockingObserve(3) {
            viewModel.loadNext()

            //test multiple load
            viewModel.loadNext()
        }

        when(status) {
            is Status.Idle -> {
                assert(status.request.page == expectedPage)
                if(!status.endReached) {
                    assert(viewModel.reviews.value?.size == api.defaultPageSize * (status.request.page + 1))
                }
                return status.request.page
            }
            is Status.Error -> {
                assert(status.request.page == expectedPage)
                return status.request.page
            }
            else -> {
                assert(false)
                return -1
            }
        }
    }

    ////////////////////////////////////////
    // TESTS
    ///////////////////////////////////////

    @Test
    fun model_load_pages() {
        viewModel = initViewModel()

        val page = testLoadNext()
        when(viewModel.status.value) {
            is Status.Idle -> assert(testLoadNext() == (page + 1))
            is Status.Error -> assert(testLoadNext() == page)
            else -> assert(false)
        }
    }

    @Test
    fun model_filter() {
        viewModel = initViewModel()

        testLoadNext()
        testLoadNext()

        assert(!viewModel.isLoading())
        assert(viewModel.page() >= 0)

        //idle, loading, idle/error
        viewModel.status.blockingObserve(3) {
            viewModel.setFilters(rating = 3)
        }

        assert(viewModel.rating() == 3)
        assert(viewModel.page() == 0)

        for(review in viewModel.data()) assert(review.rating == 3)

        testLoadNext()
        assert(viewModel.rating() == 3)
        for(review in viewModel.data()) assert(review.rating == 3)
    }

    @Test
    fun model_sort() {
        viewModel = initViewModel()

        var sortType = SORT_TYPE.WHEN
        var sortDirection = SORT_DIRECTION.DESC

        //test default
        assert(viewModel.sortType() == sortType)
        assert(viewModel.sortDirection() == sortDirection)

        //test not load without filter change
        viewModel.setFilters(sortType = sortType, sortDirection = sortDirection)
        assert(!viewModel.isLoading())

        //test change direction
        sortDirection = SORT_DIRECTION.ASC
        viewModel.setFilters(sortType = sortType, sortDirection = sortDirection)
        assert(viewModel.isLoading())
        assert(viewModel.sortDirection() == sortDirection)

        //loading idle/error
        viewModel.status.blockingObserve(2)
        assert(!viewModel.isLoading())
        assert(viewModel.sortDirection() == sortDirection)

        sortType = SORT_TYPE.RATING
        viewModel.setFilters(sortType = sortType, sortDirection = sortDirection)

        //loading idle/error
        viewModel.status.blockingObserve(2)
        assert(!viewModel.isLoading())
        assert(viewModel.sortType() == sortType)
    }
}