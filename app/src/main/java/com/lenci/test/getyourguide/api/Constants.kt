package com.lenci.test.getyourguide.api

import com.google.gson.annotations.SerializedName

enum class SORT_TYPE(val value: String) {
    @SerializedName("rating")
    RATING("rating"),

    @SerializedName("date_of_review")
    WHEN("date_of_review")
}

enum class SORT_DIRECTION(val value: String) {
    @SerializedName("desc")
    DESC("desc"),

    @SerializedName("asc")
    ASC("asc")
}

class Constants {
    companion object {
        internal const val GETYOURGUIDE_BASE_URL: String = "https://www.getyourguide.com/"
        internal const val GETYOURGUIDE_USER_AGENT: String = "GetYourGuide"

        //DEFAULTS

        const val TOUR_LOCATION = "berlin-l17"
        const val TOUR_NAME = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"

        internal const val FIRST_PAGE = 0
        internal const val DEFAULT_PAGE_SIZE: Int = 10
        internal const val DEFAULT_RATING: Int = 0

        internal val DEFAULT_SORT_BY = SORT_TYPE.WHEN
        internal val DEFAULT_SORT_DIRECTION = SORT_DIRECTION.DESC
    }
}

