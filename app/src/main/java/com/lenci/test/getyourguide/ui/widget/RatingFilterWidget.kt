package com.lenci.test.getyourguide.ui.widget

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Outline
import android.os.Build
import android.support.v7.widget.AppCompatRatingBar
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.lenci.test.getyourguide.R
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel
import android.view.MotionEvent
import android.view.ViewOutlineProvider


@SuppressLint("ClickableViewAccessibility")
class RatingFilterWidget @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    var ratingView: AppCompatRatingBar
    var icon: View

    private var model: ReviewsViewModel? = null
    private var statusObserver: (ReviewsViewModel.Status?) -> Unit  = {
        isEnabled = when(it) {
            is ReviewsViewModel.Status.Loading -> {
                false
            }
            else -> {
                updateStatus()
                true
            }
        }
    }

    ////////////////////////////////////////
    // INIT
    ///////////////////////////////////////


    init {
        orientation = HORIZONTAL
        isSelected = true
        setBackgroundResource(R.drawable.widget_sort_by_item_bg)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            outlineProvider = object : ViewOutlineProvider() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    view?.let {
                        outline?.setRoundRect(0, 0, it.width, it.height, resources.getDimensionPixelSize(R.dimen.widget_sort_by_radius).toFloat())
                    }

                }

            }
        }

        View.inflate(context, R.layout.widget_filter_rating, this)
        ratingView = findViewById(R.id.widget_filter_rating_start)


        ratingView.setOnTouchListener { v, event ->
            when(event.action) {
                MotionEvent.ACTION_DOWN -> v.isPressed = true
                MotionEvent.ACTION_CANCEL -> v.isPressed = false
                MotionEvent.ACTION_UP -> {
                    val touchPositionX = event.x
                    val width = ratingView.width
                    val starsf = touchPositionX / width * 5.0f
                    val stars = starsf.toInt() + 1

                    model?.setFilters(rating = stars)

                    v.isPressed = false
                }
            }

            true

        }
        icon = findViewById(R.id.widget_filter_rating_icon)
        icon.setOnClickListener {
            model?.setFilters(rating = 0)
        }
    }

    ////////////////////////////////////////
    // LIFECYCLE
    ///////////////////////////////////////

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        model?.status?.removeObserver(statusObserver)
    }

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    fun setReviewViewModel(model: ReviewsViewModel?) {
        this.model?.status?.removeObserver(statusObserver)

        this.model = model
        this.model?.status?.observeForever(statusObserver)

        updateStatus()
    }

    private fun updateStatus() {
        val model = this.model ?: return

        val rating = model.rating()
        when(rating) {
            0 -> {
                ratingView.rating = 0f
                icon.alpha = 0.5f
            }
            else -> {
                ratingView.rating = rating.toFloat()
                icon.alpha = 1f
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)

        alpha = when(enabled) {
            true -> 1f
            false -> 0.5f
        }

        ratingView.isEnabled = enabled
        icon.isEnabled = enabled
    }

}