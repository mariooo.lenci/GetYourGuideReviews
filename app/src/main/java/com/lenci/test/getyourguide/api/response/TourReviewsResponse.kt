package com.lenci.test.getyourguide.api.response

import com.google.gson.annotations.SerializedName
import com.lenci.test.getyourguide.api.model.Review

class TourReviewsResponse {

    @SerializedName("status")
    val status: Boolean = true

    @SerializedName("total_reviews_comments")
    val total_reviews_comments = 0

    @SerializedName("data")
    val reviews: List<Review> = ArrayList()

}