package com.lenci.test.getyourguide.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.lenci.test.getyourguide.api.*
import com.lenci.test.getyourguide.api.model.Review
import com.lenci.test.getyourguide.api.response.TourReviewsResponse
import com.lenci.test.getyourguide.ui.thread.TaskManager
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel.Status.*
import retrofit2.Response

class ReviewsViewModel(application: Application, private val api: Api, private val taskManager: TaskManager) : AndroidViewModel(application) {

    sealed class Status {
        object NotInitialized : Status()
        class Idle(val request: Request, val endReached: Boolean) : Status()
        class Error(val request: Request) : Status()
        class Loading(val request: Request) : Status()
    }

    data class Request(val page: Int, val rating: Int, val sortType: SORT_TYPE, val sortDirection: SORT_DIRECTION)

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    val reviews: MutableLiveData<ArrayList<Review>> = MutableLiveData()
    val status: MutableLiveData<Status> = MutableLiveData()

    ////////////////////////////////////////
    // INIT
    ///////////////////////////////////////

    init {
        status.value = NotInitialized
        reviews.value = ArrayList()

        loadNext()
    }

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    fun setFilters(rating: Int = rating(), sortType: SORT_TYPE = sortType(), sortDirection: SORT_DIRECTION = sortDirection()) {
        if (rating == rating() && sortType == sortType() && sortDirection == sortDirection()) {
            return
        }

        loadData(createRequest(rating = rating, sortType = sortType, sortDirection = sortDirection))
    }

    fun loadNext(): Boolean {
        val value = status.value ?: return false
        when (value) {
            is NotInitialized ->
                return loadData(createRequest())
            is Idle ->
                if (!value.endReached) return loadData(createRequest(page = page() + 1))
            is Error ->
                return loadData(createRequest())
        }

        return false
    }

    fun refreshData(): Boolean {
        if (status.value is Loading) {
            return false
        }

        return loadData(createRequest())
    }

    private fun loadData(request: Request): Boolean {
        if (status.value is Loading) {
            return false
        }

        taskManager.network {

            var response: Response<TourReviewsResponse>? = null
            try {
                response = api.service.loadTourReviews(
                        page = request.page,
                        rating = request.rating,
                        sortType = request.sortType,
                        sortDirection = request.sortDirection).execute()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val body = response?.body()

            //clear the data when requesting the first page
            if (request.page == api.firstPage) {
                reviews.value?.clear()
            }

            if (response?.isSuccessful == true && body != null) {
                val resultCount = body.reviews.size
                reviews.value?.addAll(body.reviews)
                status.postValue(Idle(request, resultCount < api.defaultPageSize))
            } else {
                status.postValue(Error(request))
            }
        }

        status.postValue(Loading(request))

        return true
    }

    //REQUEST

    private fun createRequest(page: Int = api.firstPage, rating: Int = rating(), sortType: SORT_TYPE = sortType(), sortDirection: SORT_DIRECTION = sortDirection()): Request {
        return Request(page, rating, sortType, sortDirection)
    }

    //STATUS

    fun hasData(): Boolean {
        return reviews.value?.let { !it.isEmpty() } ?: return false
    }

    fun data(): ArrayList<Review> {
        return reviews.value ?:ArrayList()
    }

    fun rating(): Int {
        val value = status.value
        return when (value) {
            is Idle -> value.request.rating
            is Error -> value.request.rating
            is Loading -> value.request.rating
            else -> 0
        }
    }

    fun sortType(): SORT_TYPE {
        val value = status.value
        return when (value) {
            is Idle -> value.request.sortType
            is Error -> value.request.sortType
            is Loading -> value.request.sortType
            else -> SORT_TYPE.WHEN
        }
    }

    fun sortDirection(): SORT_DIRECTION {
        val value = status.value
        return when (value) {
            is Idle -> value.request.sortDirection
            is Error -> value.request.sortDirection
            is Loading -> value.request.sortDirection
            else -> SORT_DIRECTION.DESC
        }
    }

    fun page(): Int {
        val value = status.value
        return when (value) {
            is Idle -> value.request.page
            is Error -> value.request.page
            is Loading -> value.request.page
            else -> api.firstPage
        }
    }

    fun isLoading(): Boolean {
        return status.value is Loading
    }


    ////////////////////////////////////////
    // FACTORY
    ///////////////////////////////////////

    class Factory(private val application: Application, private val api: Api, private val taskManager: TaskManager) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return ReviewsViewModel(application, api, taskManager) as T
        }
    }
}