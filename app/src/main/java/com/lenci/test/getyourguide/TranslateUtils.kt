package com.lenci.test.getyourguide

import android.widget.Toast
import android.content.Context
import android.content.Intent
import android.net.Uri

class TranslateUtils {

    companion object {

        fun translate(context: Context, text: String, source: String) = try {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.setPackage("com.google.android.apps.translate")

            val uri = Uri.Builder()
                    .scheme("http")
                    .authority("translate.google.com")
                    .path("/m/translate")
                    .appendQueryParameter("q", text)
                    .build()

            intent.type = "text/plain" //not needed, but possible
            intent.data = uri
            context.startActivity(intent)
        } catch (e: Exception) {
            Toast.makeText(context, "Sorry, No Google Translation Installed", Toast.LENGTH_SHORT).show()
        }

    }
}