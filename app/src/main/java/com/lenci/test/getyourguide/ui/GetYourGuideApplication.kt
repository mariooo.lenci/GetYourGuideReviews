package com.lenci.test.getyourguide.ui

import android.app.Application
import com.lenci.test.getyourguide.api.Api
import com.lenci.test.getyourguide.ui.thread.TaskManager
import com.lenci.test.getyourguide.ui.thread.ThreadExecutorTaskManager

class GetYourGuideApplication : Application() {

    companion object {
        lateinit var instance: GetYourGuideApplication private set
    }

    ////////////////////////////////////////
    // INIT
    ///////////////////////////////////////

    init {
        instance = this
    }

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    val taskManager: TaskManager = ThreadExecutorTaskManager()
    val api: Api = Api()
}