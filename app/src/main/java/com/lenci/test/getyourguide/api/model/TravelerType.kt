package com.lenci.test.getyourguide.api.model

import com.google.gson.annotations.SerializedName

enum class TravelerType(val value: String) {

    @SerializedName("couple")
    COUPLE("couple"),

    @SerializedName("friends")
    FRIENDS("friends"),

    @SerializedName("family_young")
    FAMILY_YOUNG("family_young"),

    @SerializedName("family_old")
    FAMILY_OLD("family_old"),

    @SerializedName("solo")
    SOLO("solo")

}