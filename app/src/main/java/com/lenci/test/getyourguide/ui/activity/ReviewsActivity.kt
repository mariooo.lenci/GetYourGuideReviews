package com.lenci.test.getyourguide.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatRatingBar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.lenci.test.getyourguide.R
import com.lenci.test.getyourguide.TranslateUtils
import com.lenci.test.getyourguide.api.model.Review
import com.lenci.test.getyourguide.ui.GetYourGuideApplication
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel.Status
import kotlinx.android.synthetic.main.activity_reviews.*
import kotlinx.android.synthetic.main.activity_reviews.view.*
import java.lang.ref.WeakReference


class ReviewsActivity : AppCompatActivity() {

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    lateinit var viewModel: ReviewsViewModel

    ////////////////////////////////////////
    // LIFECYCLE
    ///////////////////////////////////////

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, ReviewsViewModel.Factory(this.application, GetYourGuideApplication.instance.api, GetYourGuideApplication.instance.taskManager)).get(ReviewsViewModel::class.java)

        initUI()
        initObserver()
        supportActionBar?.subtitle = "Berlin Tempelhof Airport"
    }

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    //OBSERVER

    private fun initObserver() {
        viewModel.status.observe(this, StatusObserver(this))
    }

    //DATA

    private fun loadDataIfNecessary() {
        val layoutManager = recycler.layoutManager as LinearLayoutManager
        if (layoutManager.findLastVisibleItemPosition() == layoutManager.itemCount - 1 ) {
            viewModel.loadNext()
        }
    }

    //UI

    private fun initUI() {
        setContentView(R.layout.activity_reviews)
        setSupportActionBar(toolbar)

        sort_by.setReviewViewModel(viewModel)
        rating.setReviewViewModel(viewModel)

        swipe_to_refresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent))
        swipe_to_refresh.setOnRefreshListener {
            var refreshing = false
            if(!viewModel.isLoading()) {
                refreshing = viewModel.refreshData()
            }

            if(!refreshing) swipe_to_refresh.isRefreshing = false
        }

        recycler.adapter = ReviewsAdapter(this)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler.addOnScrollListener(ScrollListener(this))
    }

    private fun showData() {
        hideError()
        swipe_to_refresh.isRefreshing = false

        swipe_to_refresh.visibility = View.VISIBLE
        recycler.adapter.notifyDataSetChanged()

        //Check if other content should be loaded
        recycler.post { loadDataIfNecessary() }
    }

    private fun hideData() {
        swipe_to_refresh.recycler.visibility = View.VISIBLE
        recycler.adapter.notifyDataSetChanged()
    }

    private fun showContentLoader() {
        content_loading_progress.visibility = View.VISIBLE
    }

    private fun hideContentLoader() {
        content_loading_progress.visibility = View.GONE
    }

    private fun showError(@StringRes msgId: Int) {
        hideData()
        swipe_to_refresh.isRefreshing = false

        error_title.setText(msgId)
        error_group.visibility = View.VISIBLE
    }

    private fun hideError() {
        error_group.visibility = View.GONE
    }

    ////////////////////////////////////////
    // INNER CLASSES
    ///////////////////////////////////////

    //OBSERVER

    private class StatusObserver(_activity: ReviewsActivity) : Observer<Status> {

        val activity = WeakReference(_activity)

        override fun onChanged(t: Status?) {
            val status = t ?: return
            val activity = this.activity.get() ?: return

            when (status) {
                is Status.Idle -> {
                    activity.hideContentLoader()
                    if (activity.viewModel.hasData()) {
                        activity.showData()
                    } else {
                        activity.showError(R.string.error_empty)
                    }
                }
                is Status.Loading -> {
                    if (status.request.page <= 0) {
                        if(!activity.swipe_to_refresh.isRefreshing) activity.swipe_to_refresh.isRefreshing = true
                    } else {
                        activity.showContentLoader()
                    }
                }
                is Status.Error -> {
                    activity.hideContentLoader()
                    if (status.request.page <= 0) {
                        activity.showError(R.string.error_general)
                    }
                }
            }
        }
    }

    //RECYCLER

    class ScrollListener(_activity: ReviewsActivity) : RecyclerView.OnScrollListener() {

        private val activity = WeakReference(_activity)

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val activity = activity.get() ?: return
            activity.loadDataIfNecessary()
        }
    }

    private class ViewItemHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //COMPANION

        companion object {
            fun create(parent: ViewGroup): ViewItemHolder {
                return ViewItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_review, parent, false))
            }
        }

        private val title = itemView.findViewById<TextView>(R.id.review_title)
        private val date = itemView.findViewById<TextView>(R.id.review_when)
        private val rating = itemView.findViewById<AppCompatRatingBar>(R.id.review_rate)
        private val msg = itemView.findViewById<TextView>(R.id.review_message)
        private val who = itemView.findViewById<TextView>(R.id.review_who)
        private val translate = itemView.findViewById<TextView>(R.id.review_translate)

        fun setReview(review: Review) {
            if(!TextUtils.isEmpty(review.title)) {
                title.text = itemView.resources.getString(R.string.review_title, review.title)
                title.visibility = View.VISIBLE
            } else {
                title.visibility = View.GONE
            }

            date.text = review.date
            rating.rating = review.rating.toFloat()

            if(!TextUtils.isEmpty(review.message)) {
                msg.text = Html.fromHtml(review.message)
                msg.visibility = View.VISIBLE
            } else {
                msg.visibility = View.GONE
            }

            val builder = StringBuilder()
            builder.append(itemView.resources.getString(R.string.review_who, review.reviewerName))

            if(!TextUtils.isEmpty(review.reviewerCountry)) {
                builder.append(itemView.resources.getString(R.string.review_who_country, review.reviewerCountry))
            }

            who.text = builder.toString()
            when(review.foreignLanguage) {
                true -> {
                    translate.visibility = View.VISIBLE
                    translate.setOnClickListener {

                        val textBuilder = StringBuilder()
                        if(!TextUtils.isEmpty(review.title)) {
                            textBuilder.append(review.title)
                        }

                        if(!TextUtils.isEmpty(textBuilder)) {
                            textBuilder.append("\n")
                        }

                        if(!TextUtils.isEmpty(review.message)) {
                            textBuilder.append(review.message)
                        }

                        TranslateUtils.translate(it.context, textBuilder.toString(), review.languageCode)
                    }
                }
                false -> {
                    translate.visibility = View.GONE
                }
            }
        }

    }

    private class ReviewsAdapter(_activity: ReviewsActivity) : RecyclerView.Adapter<ViewItemHolder>() {

        val activity = WeakReference(_activity)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewItemHolder {
            return ViewItemHolder.create(parent)
        }

        override fun getItemCount(): Int {
            return activity.get()?.viewModel?.data()?.size ?: 0
        }

        fun getItem(position: Int): Review? {
            return activity.get()?.viewModel?.data()?.get(position)
        }

        override fun onBindViewHolder(holder: ViewItemHolder, position: Int) {
            val review = getItem(position) ?: return
            holder.setReview(review)
        }
    }


}
