package com.lenci.test.getyourguide.ui.widget

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Outline
import android.os.Build
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.LinearLayout
import android.widget.TextView
import com.lenci.test.getyourguide.R
import com.lenci.test.getyourguide.api.SORT_DIRECTION
import com.lenci.test.getyourguide.api.SORT_TYPE
import com.lenci.test.getyourguide.api.SORT_TYPE.*
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel
import com.lenci.test.getyourguide.ui.viewmodel.ReviewsViewModel.Status
import java.lang.ref.WeakReference

class SortTypeWidget @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    private var rating : TextView
    private var date : TextView
    private var clickListener : ClickListener

    private var model: ReviewsViewModel? = null
    private var statusObserver: (Status?) -> Unit  = {
        isEnabled = when(it) {
            is Status.Loading -> {
                false
            }
            else -> {
                updateStatus()
                true
            }
        }
    }

    ////////////////////////////////////////
    // INIT
    ///////////////////////////////////////

    init {
        orientation = HORIZONTAL
        clipToPadding = false
        clipChildren = false

        clickListener = ClickListener(this)

        View.inflate(context, R.layout.widget_sort_type, this)
        rating = findViewById(R.id.widget_sort_by_rating)
        rating.setOnClickListener(clickListener)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rating.clipToOutline = false
            rating.outlineProvider = object : ViewOutlineProvider() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    view?.let {
                        outline?.setRoundRect(0, 0, it.width, it.height, resources.getDimensionPixelSize(R.dimen.widget_sort_by_radius).toFloat())
                    }

                }

            }
        }

        date = findViewById(R.id.widget_sort_by_date)
        date.setOnClickListener(clickListener)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rating.clipToOutline = false
            rating.outlineProvider = object : ViewOutlineProvider() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    view?.let {
                        outline?.setRoundRect(0, 0, it.width, it.height, resources.getDimensionPixelSize(R.dimen.widget_sort_by_radius).toFloat())
                    }

                }

            }
        }
    }

    ////////////////////////////////////////
    // LIFECYCLE
    ///////////////////////////////////////

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        model?.status?.removeObserver(statusObserver)
    }

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    fun setReviewViewModel(model: ReviewsViewModel?) {
        this.model?.status?.removeObserver(statusObserver)

        this.model = model
        this.model?.status?.observeForever(statusObserver)

        updateStatus()
    }

    private fun updateStatus() {
        val model = this.model ?: return

        @DrawableRes val drawableRes = when(model.sortDirection()) {
            SORT_DIRECTION.DESC -> R.drawable.ic_arrow_down
            SORT_DIRECTION.ASC -> R.drawable.ic_arrow_up
        }

        val drawable = ContextCompat.getDrawable(context, drawableRes)


        when(model.sortType()) {
            WHEN -> {
                date.isSelected = true
                date.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
                rating.isSelected = false
                rating.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
            RATING -> {
                date.isSelected = false
                date.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                rating.isSelected = true
                rating.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)

        alpha = when(enabled) {
            true -> 1f
            false -> 0.5f
        }

        rating.isEnabled = enabled
        date.isEnabled = enabled
    }

    ////////////////////////////////////////
    // INNER CLASSES
    ///////////////////////////////////////

    private class ClickListener(_widget: SortTypeWidget) : OnClickListener {

        val widget = WeakReference(_widget)

        override fun onClick(v: View?) {
            val widget = widget.get() ?: return
            val model = widget.model ?: return


            val sortType = when(v?.id) {
                R.id.widget_sort_by_date -> WHEN
                R.id.widget_sort_by_rating -> RATING
                else -> return
            }

            model.setFilters(sortType = sortType, sortDirection = nextSortDirection(model, sortType))

            widget.updateStatus()
        }

        fun nextSortDirection(model: ReviewsViewModel, sortType : SORT_TYPE) : SORT_DIRECTION {
            if(model.sortType() != sortType)
                return model.sortDirection()

            return when(model.sortDirection()) {
                SORT_DIRECTION.DESC -> SORT_DIRECTION.ASC
                SORT_DIRECTION.ASC -> SORT_DIRECTION.DESC
            }
        }
    }

}