package com.lenci.test.getyourguide.api

import com.lenci.test.getyourguide.BuildConfig
import com.lenci.test.getyourguide.api.model.Review
import com.lenci.test.getyourguide.api.response.TourReviewsResponse
import com.lenci.test.getyourguide.api.utils.EnumConverterFactory
import com.lenci.test.getyourguide.api.utils.UserAgentInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GetYourGuideService {

    @GET("/{location}/{tour}/reviews.json")
    fun loadTourReviews(
            @Path("location") location: String = Constants.TOUR_LOCATION,
            @Path("tour") tour: String = Constants.TOUR_NAME,
            @Query("count") page_size: Int = Constants.DEFAULT_PAGE_SIZE,
            @Query("page") page: Int = Constants.FIRST_PAGE,
            @Query("rating") rating: Int = Constants.DEFAULT_RATING,
            @Query("sortBy") sortType: SORT_TYPE = Constants.DEFAULT_SORT_BY,
            @Query("direction") sortDirection: SORT_DIRECTION = Constants.DEFAULT_SORT_DIRECTION
    ): Call<TourReviewsResponse>

}

class Api {

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    private val retrofit: Retrofit = initRetrofit()
    val service: GetYourGuideService = retrofit.create(GetYourGuideService::class.java)

    val defaultPageSize = Constants.DEFAULT_PAGE_SIZE
    val firstPage = Constants.FIRST_PAGE

    ////////////////////////////////////////
    // INIT
    ///////////////////////////////////////

    private fun initRetrofit(): Retrofit {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.addInterceptor(UserAgentInterceptor(Constants.GETYOURGUIDE_USER_AGENT))

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(interceptor)
        }

        return Retrofit.Builder()
                .baseUrl(Constants.GETYOURGUIDE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(EnumConverterFactory())
                .client(httpClientBuilder.build()).build()
    }

}