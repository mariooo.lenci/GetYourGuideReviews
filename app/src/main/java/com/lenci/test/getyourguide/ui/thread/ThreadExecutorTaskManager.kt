package com.lenci.test.getyourguide.ui.thread

import android.os.Handler
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import android.os.Looper

class ThreadExecutorTaskManager : TaskManager {

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    val diskExecutor = Executors.newSingleThreadExecutor()
    val networkExecutor = Executors.newFixedThreadPool(3)
    val mainThreadExecutor = MainThreadExecutor()

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    override fun network(task: () -> Unit) {
        networkExecutor.execute(task)
    }

    override fun disk(task: () -> Unit) {
        diskExecutor.execute(task)
    }

    override fun mainThread(task: () -> Unit) {
        mainThreadExecutor.execute(task)
    }

    ////////////////////////////////////////
    // INNER CLASSES
    ///////////////////////////////////////

    class MainThreadExecutor : Executor {

        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}