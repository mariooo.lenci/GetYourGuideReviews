package com.lenci.test.getyourguide.api.model

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

class Review {

    ////////////////////////////////////////
    // COMPANION
    ///////////////////////////////////////

    companion object {
        val dateFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())
    }

    ////////////////////////////////////////
    // ATTRIBUTES
    ///////////////////////////////////////

    @SerializedName("review_id")
    var id: Int = -1

    @SerializedName("rating")
    var rating: Int = 0

    @SerializedName("title")
    var title: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("travler_type")
    val traveler_type: TravelerType? = null

    @SerializedName("reviewerName")
    var reviewerName = ""

    @SerializedName("reviewerCountry")
    var reviewerCountry = ""

    @SerializedName("date")
    var date = ""

    @SerializedName("lenguageCode")
    var languageCode = ""

    @SerializedName("foreignLanguage")
    var foreignLanguage: Boolean = false

    ////////////////////////////////////////
    // BODY
    ///////////////////////////////////////

    fun getDateTimestamp() : Long {
         return getDate().time
    }

    fun getDate() : Date {
        return dateFormat.parse(date)
    }

}