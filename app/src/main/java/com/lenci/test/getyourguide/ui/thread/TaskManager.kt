package com.lenci.test.getyourguide.ui.thread

interface TaskManager {

    fun network(task: () -> Unit)

    fun disk(task: () -> Unit)

    fun mainThread(task: () -> Unit)

}