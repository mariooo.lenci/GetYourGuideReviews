package com.lenci.test.getyourguide.api.utils

import okhttp3.Interceptor
import okhttp3.Response

class UserAgentInterceptor(private val userAgent: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        return chain?.let {
            val originalRequest = chain.request()
            val requestWithUserAgent = originalRequest.newBuilder().header("User-Agent", userAgent).build()

            chain.proceed(requestWithUserAgent)
        } ?: throw IllegalArgumentException()

    }

}