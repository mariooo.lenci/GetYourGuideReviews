# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#RETROFIT

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8

# OKHTTP

-keep class com.squareup.okhttp3.** { *; }
-dontwarn okhttp3.internal.platform.**
-dontwarn com.squareup.okhttp3.**
-dontwarn okio.**

#GOOGLE

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

#MODELS
-keep class com.lenci.test.getyourguide.api.model { *; }
-keep class com.lenci.test.getyourguide.api.response.** { *; }
-keep class com.lenci.test.getyourguide.api.SORT_TYPE { *; }
-keep class com.lenci.test.getyourguide.api.SORT_DIRECTION { *; }

#KOTLIN
-keep class kotlin.jvm.internal.**
-keep class kotlin.jvm.functions.Function1
-keep class kotlin.Function
-keep class kotlin.internal.** { *; }

#ANNOTATION
-keep class javax.annotation.** { *; }
-dontwarn javax.annotation.**


