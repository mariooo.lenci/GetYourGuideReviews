# GetYourGuide Code Challente

 Applicant: **Mario Lenci**

 - [LinkedIn]
 - [Github] 
 - [CV] 
 - phone number: **+393403742204**
 - Skype: **f4milyfriend**

# Assignment

Create a Android app that allows a customer to browse reviews for one of our most popular Berlin tours.  Feel free to add any feature that you feel relevant to the use case.

The following web service delivers n (count) reviews which include the review author, title, message, date, rating, language code:

>https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json?count=5&page=0&rating=0&sortBy=date_of_review&direction=DESC

Required URL params
```
count=[integer]
page=[integer]
```

Optional URL params
```
rating=[integer 0..5]
sortBy=[string date_of_review/rating]
direction=[string asc/desc]
```

Note: For this endpoint to give a valid response you need to change the user agent in the request header to “GetYourGuide”

# Project Relevant File

- [APK]
- [keystore]

# App Relevant Information
**MAIN LIBRARIES**

- Language: **Kotlin 1.2.60** 
- Networking: **Retrofit 2.4.0**
- Archtechture: **android.arch.lifecycle:extensions v1.1.1** 
- Support: **com.android.support.\*  27.1.1**

**COMPONENTS**

I used the MVVM patter to implement the application. The Main components are:
- **Model** - [Api]
- **View** - [ReviewsActivity]
- **ViewModel** - [ReviewsViewModel]

**FEATURE**

The App let the user browse the reviews of the *Berlin Tempelhof Airport: The Legend of Tempelhof* tour on the [GetYourGuide] website.
The user other than having a single endless list of reviews can also:
- Sort the by reating or date 
- Filter the reviews by the rating value of the user
- Translate the reviews that aren't in english opening the *Google Translate* App implemented through the exposed Intent
- Even if not requested the application support configuaration changes. The feature comes for free using the MVVM patter with the lifecycle components provided by the [Jetpack Library]  

**TODO FEAUTRE**

Due to time constraint I could implement all the features I thought of, here are some of them:
- Filter the reviews by the type of the reviewer ( the api were not provided but on the website is possible, so I could porbably do the same )
- Translate the review messages in loco. I looked online which are the possible services one could use to implement this feature. The Google Translate API seems to be the most pouplar, but there is not any easy solution so I decieded to go with the Intent solution.

**TESTS**

Due to time contraint I implemented just a few Unit Test:
- TestAPI
- TestReviewsViewModel
- **TODO**: InstrumentedTestReviewsActivity 

[apk]: https://gitlab.com/mariooo.lenci/GetYourGuideReviews/blob/master/APK/GetYourGuide%20Test%20-%20Mario%20Lenci.apk
[keystore]: https://gitlab.com/mariooo.lenci/GetYourGuideReviews/blob/master/keystore/getyourguide 
[api]:  https://gitlab.com/mariooo.lenci/GetYourGuideReviews/blob/master/app/src/main/java/com/lenci/test/getyourguide/api/Api.kt
[ReviewsActivity]:  https://gitlab.com/mariooo.lenci/GetYourGuideReviews/blob/master/app/src/main/java/com/lenci/test/getyourguide/ui/activity/ReviewsActivity.kt
[ReviewsViewModel]: https://gitlab.com/mariooo.lenci/GetYourGuideReviews/blob/master/app/src/main/java/com/lenci/test/getyourguide/ui/viewmodel/ReviewsViewModel.kt
[jetpack library]: https://developer.android.com/jetpack/
[getyourguide]: https://www.getyourguide.com/
[github]: https://github.com/MarioLenci
[linkedin]: https://www.linkedin.com/in/mario-lenci/
[cv]: https://www.visualcv.com/8_aoqg5wddw